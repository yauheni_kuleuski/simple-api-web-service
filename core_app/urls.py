from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('api/', include('api_customers.urls')),
    path('', include('web_interface.urls'))
]
