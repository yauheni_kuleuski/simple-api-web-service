from django.urls import path
from web_interface.views import main_menu

urlpatterns = [
    path('', main_menu)
]
