from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length=80)
    email = models.EmailField()
    created_ad = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
