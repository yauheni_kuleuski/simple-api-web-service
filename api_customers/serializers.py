from rest_framework import serializers
from api_customers.models import Customer


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ['name', 'email', 'created_ad']
