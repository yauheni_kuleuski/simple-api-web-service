It is just a very simple API service for DevOps and QA Automation practice.

Instruction for run `"Simple API WEB Service"`:

1. Create invorement
```bash
cd ~/
mkdir code
cd code
virtualenv venv --python=python3
source venv/bin/activate
```
2. Clone remote repository on local machine (using SSH):
```git
git clone git@gitlab.com:yauheni_kuleuski/simple-api-web-service.git
```
3. Install needed requirements:
```python
cd simple-api-web-service/
pip install -r requirements.txt
```
4. Run server
```django
python namage.py makemigrations
python manage.py migrate
python manage.py runserver
```
5. Check in browser
```
http://localhost:8000/
```

At this moment this service avalible by address: http://37.228.116.193/api